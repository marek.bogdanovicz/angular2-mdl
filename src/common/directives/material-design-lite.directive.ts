"use strict";

import {Directive, AfterViewInit} from "angular2/core";
declare let componentHandler: any;

@Directive({
    selector: "[mdl]"
})
export class MDL implements AfterViewInit {
    ngAfterViewInit() {
        componentHandler.upgradeAllRegistered();
    }
}
