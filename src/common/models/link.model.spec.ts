import {
  describe,
  it
} from 'angular2/testing';
import {Link} from "./link.model";

export function main() {
  describe('Link model', () => {

    let user: Link = new Link();
    user.id = "1";
    user.text = "test";

    it("Link model - should have.", () => {
      expect(user.id).toBe("1");
      expect(user.text).toBe("test");
    });
  });
}

