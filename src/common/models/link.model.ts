/**
 * Used for linking with anchors
 */

export class Link {
  public id: string;
  public text: string;
}
