"use strict";

// import Angular 2
import {Component} from "angular2/core";

// import Angular 2 Component Router
import {RouteConfig, RouterOutlet, RouterLink} from "angular2/router";

// app page components
import {PageComponent} from "../pages/page/page.component";

@Component({
	selector: "app",
	template: "<router-outlet></router-outlet>",
	directives: [RouterOutlet, RouterLink]
})
@RouteConfig([
	{ path: "/", component: PageComponent, name: "PageComponent", useAsDefault:true}
])
export class App {}
