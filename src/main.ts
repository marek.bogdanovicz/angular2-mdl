import {bootstrap} from 'angular2/platform/browser';
import {App} from './core/app';

import {provide, enableProdMode, ComponentRef} from "angular2/core";
import {HTTP_PROVIDERS} from "angular2/http";


import "rxjs/Rx";

// import Angular 2 Component Router
// reference: http://blog.thoughtram.io/angular/2015/06/16/routing-in-angular-2.html
import {LocationStrategy, HashLocationStrategy, ROUTER_PROVIDERS} from "angular2/router";


if ('<%= ENV %>' === 'prod') { enableProdMode(); }


bootstrap(App, [
  //appServicesInjectables, // alternative way of filling the injector with all the classes we want to be able to inject
  ROUTER_PROVIDERS,
  HTTP_PROVIDERS,
  //ELEMENT_PROBE_PROVIDERS, // remove in production
  provide(LocationStrategy, { useClass: HashLocationStrategy }) 
]).then((appRef: ComponentRef) => {
    // store a reference to the application injector
  }
);


