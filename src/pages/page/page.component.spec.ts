import {
  TestComponentBuilder,
  describe,
  injectAsync,
  ComponentFixture,
  it
} from 'angular2/testing';
import {PageComponent} from "./page.component";



export function main() {
  describe('Page Component', () => {
    it('PageComponent - Should work.',
      injectAsync([TestComponentBuilder], (tcb: TestComponentBuilder) => {
        return tcb
          .createAsync(PageComponent)
          .then((component: ComponentFixture) => {
            expect(component.nativeElement.querySelector(".h4")).toBeDefined();
          });
      }));
  });
}
