"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {MDL} from "../../common/directives/material-design-lite.directive";
import {TestComponent} from "../../components/test/test.component";

@Component({
	selector: "page",
	templateUrl: "pages/page/page.component.html",
  directives: [MDL, TestComponent]
})
export class PageComponent {
	title:string = "Page Component";
}
